package weverson.familymap.models;

/**
 * Created by Weverson on 3/27/2016.
 */
public class User {
    private String hostIp;
    private String port;
    private String authorizationSer;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAuthorizationSer() {
        return authorizationSer;
    }

    public void setAuthorizationSer(String authorizationSer) {
        this.authorizationSer = authorizationSer;
    }
}
