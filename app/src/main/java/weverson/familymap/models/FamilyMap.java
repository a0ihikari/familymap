package weverson.familymap.models;

import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weverson.familymap.views.Activity.MainActivity;

/**
 * Created by Weverson on 3/27/2016.
 */
public class FamilyMap {
    private List<Event> events;
    private List<People> people;
    private List<Event> filterdEvents = new ArrayList<>();
    private User user;
    private Event event = new Event("NoEvent");
    private People person;
    private Map<String, Integer> types = new HashMap<String, Integer>();
    private Map<String, Boolean> filterTypes = new HashMap<String, Boolean>();
    public static final FamilyMap SINGLETON = new FamilyMap();

    public void filter (String type){
        for (int i = 0; i < getEvents().size(); i++){
            if (getEvents().get(i).getDescription().equals(type)){
                filterdEvents.add(getEvents().get(i));
                getEvents().remove(i);
                i--;
            }
        }
    }

    public void filterInitializer(){
        for (String type : types.keySet()){
            filterTypes.put(type,false);
        }
        filterTypes.put("father",false);
        filterTypes.put("mother",false);
        filterTypes.put("male",false);
        filterTypes.put("female",false);
    }

    public Map<String, Boolean> getFilterTypes() {
        return filterTypes;
    }

    public void setFilterTypes(Map<String, Boolean> filterTypes) {
        this.filterTypes = filterTypes;
    }

    public Map<String, Integer> getTypes() {
        return types;
    }

    public void setTypes(Map<String, Integer> types) {
        this.types = types;
    }

    public List<People> getPeople() {
        return people;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public void setPeople(List<People> people) {
        this.people = people;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
