package weverson.familymap.views.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import java.util.ArrayList;
import java.util.List;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Recycler.Adapters.FilterAdapter;
import weverson.familymap.views.Recycler.Adapters.SearchAdapter;
import weverson.familymap.views.Recycler.Items.FilterItems;
import weverson.familymap.views.Recycler.Items.SearchItems;

public class FilterActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private FilterAdapter adapter;
    private List<FilterItems> listItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycle_filter);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        updateList();
    }

    public void updateList(){

        adapter = new FilterAdapter(FilterActivity.this, listItems);
        mRecyclerView.setAdapter(adapter);

        adapter.clearAdapter();

        FilterItems item;
        for (String type : FamilyMap.SINGLETON.getTypes().keySet()){
            item = new FilterItems();
            item.setStitle(type + " Events");
            item.setStext("FILTER BY " + type + " EVENTS");
            item.setSwitch(FamilyMap.SINGLETON.getFilterTypes().get(type));

            listItems.add(item);
        }
        item = new FilterItems();
        item.setStitle("Father's Side");
        item.setStext("FILTER BY FATHER'S SIDE OF FAMILY");
        item.setSwitch(FamilyMap.SINGLETON.getFilterTypes().get("father"));
        listItems.add(item);
        item = new FilterItems();
        item.setStitle("Mother's Side");
        item.setStext("FILTER BY MOTHER'S SIDE OF FAMILY");
        item.setSwitch(FamilyMap.SINGLETON.getFilterTypes().get("mother"));
        listItems.add(item);
        item = new FilterItems();
        item.setStitle("Male Events");
        item.setStext("FILTER BY EVENTS BASED ON GENDER");
        item.setSwitch(FamilyMap.SINGLETON.getFilterTypes().get("male"));
        listItems.add(item);
        item = new FilterItems();
        item.setStitle("Female Events");
        item.setStext("FILTER BY EVENTS BASED ON GENDER");
        item.setSwitch(FamilyMap.SINGLETON.getFilterTypes().get("female"));
        listItems.add(item);

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_other, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent itemIntent;
        if (id == R.id.action_up) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
