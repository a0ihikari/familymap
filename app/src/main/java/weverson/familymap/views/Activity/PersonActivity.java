package weverson.familymap.views.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Expandable.ExAdapter;

public class PersonActivity extends AppCompatActivity {
    ExpandableListView expandableListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        TextView textView = (TextView)findViewById(R.id.person_first);
        textView.setText(FamilyMap.SINGLETON.getPerson().getFirstName());
        textView = (TextView)findViewById(R.id.person_last);
        textView.setText(FamilyMap.SINGLETON.getPerson().getLastName());
        textView = (TextView)findViewById(R.id.person_gender);
        if (FamilyMap.SINGLETON.getPerson().getGender().equals("m")){
            textView.setText("Male");
        } else {
            textView.setText("Female");
        }

        expandableListView = (ExpandableListView)findViewById(R.id.person_ex);
        List<String> headings = new ArrayList<>();
        headings.add("LIFE EVENTS");
        headings.add("FAMILY");
        ExAdapter exAdapter = new ExAdapter(this,headings);
        expandableListView.setAdapter(exAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_map, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent itemIntent;
        if (id == R.id.action_up) {
            finish();
            return true;
        } else if (id == R.id.action_top){
            itemIntent = new Intent(this, MainActivity.class);
            itemIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(itemIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
