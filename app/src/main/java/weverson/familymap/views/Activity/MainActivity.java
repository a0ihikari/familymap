package weverson.familymap.views.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import weverson.familymap.R;
import weverson.familymap.tasks.LoginTask;
import weverson.familymap.views.Fragments.MapFragment;

public class MainActivity extends AppCompatActivity {

    private Button mSignInButton;
    private EditText mUserName;
    private EditText mPassword;
    private EditText mHost;
    private EditText mPort;
    private String userName;
    private String password;
    private String hostIp;
    private String port;

    private void setupVariables(){
        mUserName = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);
        mHost = (EditText) findViewById(R.id.host);
        mPort = (EditText) findViewById(R.id.port);
        mSignInButton = (Button) findViewById(R.id.sign_in_button);
    }

    private void addMapFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        MapFragment fragment = new MapFragment();
        transaction.add(R.id.mapView, fragment);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupVariables();

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userName = mUserName.getText().toString(); //get the username
                password = mPassword.getText().toString();
                hostIp = mHost.getText().toString(); //get the host IP
                port = mPort.getText().toString();

                try {
                    URL url = new URL("http://" + hostIp + ":" + port + "/user/login");
                    LoginTask task = new LoginTask();
                    task.setContext(getApplicationContext());
                    task.setLogin(userName, password, hostIp, port);
                    task.execute(url);
                    String wait = task.get();
                    if(!wait.contains("message")){
                        setContentView(R.layout.map_fragment);
                        addMapFragment();
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
