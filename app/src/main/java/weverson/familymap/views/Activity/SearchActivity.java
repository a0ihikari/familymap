package weverson.familymap.views.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import java.util.ArrayList;
import java.util.List;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Recycler.Adapters.SearchAdapter;
import weverson.familymap.views.Recycler.Items.SearchItems;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private SearchAdapter adapter;
    private List<SearchItems> listItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycle_search);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        updateList();
    }

    public void updateList(){

        adapter = new SearchAdapter(SearchActivity.this, listItems);
        mRecyclerView.setAdapter(adapter);

        adapter.clearAdapter();

        SearchItems item;
        for (int i = 0; i < FamilyMap.SINGLETON.getPeople().size(); i++){
            Drawable genderIcon;
            item = new SearchItems();
            if (FamilyMap.SINGLETON.getPeople().get(i).getGender().equals("m")){
                genderIcon = new IconDrawable(this, Iconify.IconValue.fa_male)
                        .colorRes(R.color.colorPrimary).sizeDp(40);
            } else {
                genderIcon = new IconDrawable(this, Iconify.IconValue.fa_female)
                        .colorRes(R.color.colorAccent).sizeDp(40);
            }
            item.setImage(genderIcon);
            item.setStext(FamilyMap.SINGLETON.getPeople().get(i).getFirstName() + " " +
                    FamilyMap.SINGLETON.getPeople().get(i).getLastName());

            listItems.add(item);
        }
        for (int i = 0; i < FamilyMap.SINGLETON.getEvents().size(); i++){
            Drawable mapMarker= new IconDrawable(this, Iconify.IconValue.fa_map_marker)
                    .colorRes(R.color.colorGray).sizeDp(40);
            item = new SearchItems();
            item.setImage(mapMarker);
            for (int j = 0; j < FamilyMap.SINGLETON.getPeople().size(); j++) {
                if (FamilyMap.SINGLETON.getPeople().get(j).getPersonID().equals(
                        FamilyMap.SINGLETON.getEvents().get(i).getPersonID())) {
                    item.setStext(FamilyMap.SINGLETON.getEvents().get(i).getDescription() + ": " +
                                    FamilyMap.SINGLETON.getEvents().get(i).getCity() + " - " +
                                    FamilyMap.SINGLETON.getEvents().get(i).getCountry()+ " (" +
                                    FamilyMap.SINGLETON.getEvents().get(i).getYear()+ ")\n" +
                                    FamilyMap.SINGLETON.getPeople().get(j).getFirstName() + " " +
                                    FamilyMap.SINGLETON.getPeople().get(j).getLastName());
                    item.setImage(mapMarker);

                    listItems.add(item);
                    break;
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_other, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent itemIntent;
        if (id == R.id.action_up) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
