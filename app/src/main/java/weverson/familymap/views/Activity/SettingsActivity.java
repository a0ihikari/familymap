package weverson.familymap.views.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import weverson.familymap.R;
import weverson.familymap.tasks.LoginTask;

public class SettingsActivity extends AppCompatActivity {

    Spinner spinnerLife;
    ArrayAdapter<CharSequence> adapterLife;
    Spinner spinnerTree;
    ArrayAdapter<CharSequence> adapterTree;
    Spinner spinnerSpouse;
    ArrayAdapter<CharSequence> adapterSpouse;
    Spinner spinnerType;
    ArrayAdapter<CharSequence> adapterType;
    Switch switchLife;
    Switch switchTree;
    Switch switchSpouse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        spinnerLife = (Spinner)findViewById(R.id.spinner_life);
        adapterLife = ArrayAdapter.createFromResource(this,R.array.spinner_life,
                android.R.layout.simple_spinner_dropdown_item);
        adapterLife.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLife.setAdapter(adapterLife);
        spinnerLife.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) +
                        " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerTree = (Spinner)findViewById(R.id.spinner_tree);
        adapterTree = ArrayAdapter.createFromResource(this,R.array.spinner_life,
                android.R.layout.simple_spinner_dropdown_item);
        adapterTree.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTree.setAdapter(adapterTree);
        spinnerTree.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) +
                        " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSpouse = (Spinner)findViewById(R.id.spinner_spouse);
        adapterSpouse = ArrayAdapter.createFromResource(this,R.array.spinner_life,
                android.R.layout.simple_spinner_dropdown_item);
        adapterSpouse.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSpouse.setAdapter(adapterSpouse);
        spinnerSpouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) +
                        " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerType = (Spinner)findViewById(R.id.spinner_type);
        adapterType = ArrayAdapter.createFromResource(this,R.array.spinner_life,
                android.R.layout.simple_spinner_dropdown_item);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapterType);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) +
                        " selected", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        switchLife = (Switch)findViewById(R.id.switch_life);
        switchLife.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getBaseContext(), "Checked", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getBaseContext(), "Unchecked", Toast.LENGTH_LONG).show();
                }
            }
        });
        switchTree = (Switch)findViewById(R.id.switch_tree);
        switchTree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Toast.makeText(getBaseContext(), "Checked", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getBaseContext(), "Unchecked", Toast.LENGTH_LONG).show();
                }
            }
        });
        switchSpouse = (Switch)findViewById(R.id.switch_spouse);
        switchSpouse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getBaseContext(), "Checked", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getBaseContext(), "Unchecked", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_other, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent itemIntent;
        if (id == R.id.action_up) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
