package weverson.familymap.views.Expandable;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArraySet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import com.google.android.gms.plus.model.people.Person;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Activity.PersonActivity;
import weverson.familymap.views.Recycler.Adapters.PersonAdapter;
import weverson.familymap.views.Recycler.Adapters.SearchAdapter;
import weverson.familymap.views.Recycler.Items.PersonItems;
import weverson.familymap.views.Recycler.Items.SearchItems;

/**
 * Created by Weverson on 4/11/2016.
 */
public class ExAdapter extends BaseExpandableListAdapter{

    private Context context;
    private List<String> header_titles;
    private RecyclerView mRecyclerView;
    private List<PersonItems> listItems = new ArrayList<>();

    public ExAdapter (Context _context, List<String> _header_titles){
        context = _context;
        header_titles = _header_titles;
    }

    @Override
    public int getGroupCount() {
        return header_titles.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return header_titles.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String title = (String)this.getGroup(groupPosition);

            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.person_parent, null);

        TextView textView = (TextView)convertView.findViewById(R.id.ex_heading);
        textView.setText(title);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.child_recycler, null);
            mRecyclerView = (RecyclerView) convertView.findViewById(R.id.recycle_child);

            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            mRecyclerView.setLayoutManager(linearLayoutManager);

            updateList(header_titles.get(groupPosition));

        return convertView;
    }

    public void updateList(String title){

        PersonAdapter adapter = new PersonAdapter(context, listItems);
        mRecyclerView.setAdapter(adapter);

        adapter.clearAdapter();

        PersonItems item;
        if (title.equals("LIFE EVENTS")){
            for (int i = 0; i < FamilyMap.SINGLETON.getEvents().size(); i++){
                if (FamilyMap.SINGLETON.getEvents().get(i).getPersonID()
                        .equals(FamilyMap.SINGLETON.getPerson().getPersonID())){
                    Drawable mapMarker= new IconDrawable(context, Iconify.IconValue.fa_map_marker)
                            .colorRes(R.color.colorGray).sizeDp(40);
                    item = new PersonItems();
                    item.setImage(mapMarker);
                    item.setStext(FamilyMap.SINGLETON.getEvents().get(i).getDescription() + ": " +
                            FamilyMap.SINGLETON.getEvents().get(i).getCity() + " - " +
                            FamilyMap.SINGLETON.getEvents().get(i).getCountry() + " (" +
                            FamilyMap.SINGLETON.getEvents().get(i).getYear() + ")\n" +
                            FamilyMap.SINGLETON.getPerson().getFirstName() + " " +
                            FamilyMap.SINGLETON.getPerson().getLastName());
                    item.setImage(mapMarker);
                    item.setEvent(FamilyMap.SINGLETON.getEvents().get(i));
                    listItems.add(item);
                }
            }
            List<Integer> years = new ArrayList<>();

            Map<Integer, List<PersonItems>> sortYears = new HashMap<>();
            for(int i = 0; i < listItems.size(); i++){
                StringBuilder year = new StringBuilder();
                for(Character character : listItems.get(i).getStext().toCharArray()){

                    if (Character.isDigit(character)){
                        year.append(character);
                    }
                }
                if(!sortYears.containsKey(Integer.parseInt(year.toString()))){
                    List<PersonItems> list = new ArrayList<>();
                    sortYears.put(Integer.parseInt(year.toString()),list);
                    sortYears.get(Integer.parseInt(year.toString())).add(listItems.get(i));
                } else {
                    sortYears.get(Integer.parseInt(year.toString())).add(listItems.get(i));
                }
                years.add(Integer.parseInt(year.toString()));
            }
            Set<Integer> sortYear = new HashSet<>();
            sortYear.addAll(years);
            years.clear();
            years.addAll(sortYear);
            Collections.sort(years);
            int position = 0;
            for(int i = 0; i < years.size(); i++){

                for(int j = 0; j < sortYears.get(years.get(i)).size(); j++) {
                    if(j > 0){
                        position++;
                    }
                    listItems.set(position, sortYears.get(years.get(i)).get(j));
                }
                position++;
            }

        } else {
            for (int i = 0; i < FamilyMap.SINGLETON.getPeople().size(); i++){
                Drawable genderIcon;
                item = new PersonItems();
                if (FamilyMap.SINGLETON.getPeople().get(i).getPersonID()
                        .equals(FamilyMap.SINGLETON.getPerson().getFather())){
                    genderIcon = new IconDrawable(context, Iconify.IconValue.fa_male)
                            .colorRes(R.color.colorPrimary).sizeDp(40);
                    item.setImage(genderIcon);
                    item.setStext(FamilyMap.SINGLETON.getPeople().get(i).getFirstName() + " " +
                            FamilyMap.SINGLETON.getPeople().get(i).getLastName() + "\nFather");
                    item.setPeople(FamilyMap.SINGLETON.getPeople().get(i));

                    listItems.add(item);
                } else if(FamilyMap.SINGLETON.getPeople().get(i).getPersonID()
                        .equals(FamilyMap.SINGLETON.getPerson().getMother())){
                    genderIcon = new IconDrawable(context, Iconify.IconValue.fa_female)
                            .colorRes(R.color.colorAccent).sizeDp(40);
                    item.setImage(genderIcon);
                    item.setStext(FamilyMap.SINGLETON.getPeople().get(i).getFirstName() + " " +
                            FamilyMap.SINGLETON.getPeople().get(i).getLastName() + "\nMother");
                    item.setPeople(FamilyMap.SINGLETON.getPeople().get(i));

                    listItems.add(item);
                } else if(FamilyMap.SINGLETON.getPeople().get(i).getPersonID()
                        .equals(FamilyMap.SINGLETON.getPerson().getSpouse())){
                    if (FamilyMap.SINGLETON.getPeople().get(i).getGender().equals("m")){
                        genderIcon = new IconDrawable(context, Iconify.IconValue.fa_male)
                                .colorRes(R.color.colorPrimary).sizeDp(40);
                    } else {
                        genderIcon = new IconDrawable(context, Iconify.IconValue.fa_female)
                                .colorRes(R.color.colorAccent).sizeDp(40);
                    }
                    item.setImage(genderIcon);
                    item.setStext(FamilyMap.SINGLETON.getPeople().get(i).getFirstName() + " " +
                            FamilyMap.SINGLETON.getPeople().get(i).getLastName() + "\nSpouse");
                    item.setPeople(FamilyMap.SINGLETON.getPeople().get(i));

                    listItems.add(item);
                } else if(FamilyMap.SINGLETON.getPerson().getPersonID()
                        .equals(FamilyMap.SINGLETON.getPeople().get(i).getFather()) ||
                        FamilyMap.SINGLETON.getPerson().getPersonID()
                        .equals(FamilyMap.SINGLETON.getPeople().get(i).getMother())){
                    if (FamilyMap.SINGLETON.getPeople().get(i).getGender().equals("m")){
                        genderIcon = new IconDrawable(context, Iconify.IconValue.fa_male)
                                .colorRes(R.color.colorPrimary).sizeDp(40);
                    } else {
                        genderIcon = new IconDrawable(context, Iconify.IconValue.fa_female)
                                .colorRes(R.color.colorAccent).sizeDp(40);
                    }
                    item.setImage(genderIcon);
                    item.setStext(FamilyMap.SINGLETON.getPeople().get(i).getFirstName() + " " +
                            FamilyMap.SINGLETON.getPeople().get(i).getLastName() + "\nChild");
                    item.setPeople(FamilyMap.SINGLETON.getPeople().get(i));

                    listItems.add(item);
                }
            }
            }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
