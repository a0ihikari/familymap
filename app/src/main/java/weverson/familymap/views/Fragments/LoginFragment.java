package weverson.familymap.views.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;

/**
 * Created by Weverson on 3/18/2016.
 */
public class LoginFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            FamilyMap.SINGLETON.getEvent().setEventID("NoEvent");
            return inflater.inflate(R.layout.login_fragment, container, false);
        }
}
