package weverson.familymap.views.Fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import weverson.familymap.R;
import weverson.familymap.models.Event;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.models.People;
import weverson.familymap.tasks.EventTask;
import weverson.familymap.tasks.PeopleTask;
import weverson.familymap.views.Activity.FilterActivity;
import weverson.familymap.views.Activity.MainActivity;
import weverson.familymap.views.Activity.SearchActivity;
import weverson.familymap.views.Activity.SettingsActivity;
import weverson.familymap.views.Recycler.Adapters.MapAdapter;
import weverson.familymap.views.Recycler.Items.MapItem;

/**
 * A fragment that launches other parts of the demo application.
 */
public class MapFragment extends Fragment {
    private String latitude1;
    private String longitude1;
    private MapView mMapView;
    private GoogleMap googleMap;
    private RecyclerView mRecyclerView;
    private MapAdapter adapter;
    private List<MapItem> listItems = new ArrayList<>();

    private  void getMarkers(Map<String, Integer> types){
        double latitude;
        double longitude;
        for (int i = 0; i < FamilyMap.SINGLETON.getEvents().size(); i++) {
            latitude = Double.parseDouble(FamilyMap.SINGLETON.getEvents().get(i).getLatitude());
            longitude = Double.parseDouble(FamilyMap.SINGLETON.getEvents().get(i).getLongitude());

            // create marker
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude)).title(FamilyMap.SINGLETON.getEvents().get(i).getEventID());

            // Changing marker icon
            switch (types.get(FamilyMap.SINGLETON.getEvents().get(i).getDescription())) {
                case 0:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    break;
                case 1:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                    break;
                case 2:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
                    break;
                case 3:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    break;
                case 4:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    break;
                case 5:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                    break;
                case 6:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    break;
                case 7:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                    break;
                case 8:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                    break;
                case 9:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                    break;
                default:
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                    break;
            }

            // adding marker
            googleMap.addMarker(marker);
        }
    }

    private void getEvents() {
        int color = 0;

        URL url = null;
        try {
            url = new URL("http://" + FamilyMap.SINGLETON.getUser().getHostIp() + ":" +
                    FamilyMap.SINGLETON.getUser().getPort() + "/event/");
            EventTask etask = new EventTask();
            etask.setAuthorizationToken(FamilyMap.SINGLETON.getUser().getAuthorizationSer());
            etask.execute(url);
            String wait = etask.get();
            if (!wait.contains("message")) {
                JSONObject elements = new JSONObject(wait);
                JSONArray elementsList;
                List<Event> events = new ArrayList<>();
                Map<String, Integer> types = new HashMap<String, Integer>();

                elementsList = elements.getJSONArray("data");
                for (int i = 0; i < elementsList.length(); i++) {
                    Event event = new Event();
                    event.setEventID(elementsList.getJSONObject(i).getString("eventID"));
                    event.setPersonID(elementsList.getJSONObject(i).getString("personID"));
                    event.setLatitude(elementsList.getJSONObject(i).getString("latitude"));
                    event.setLongitude(elementsList.getJSONObject(i).getString("longitude"));
                    event.setCountry(elementsList.getJSONObject(i).getString("country"));
                    event.setCity(elementsList.getJSONObject(i).getString("city"));
                    event.setDescription(elementsList.getJSONObject(i).getString("description"));
                    event.setYear(elementsList.getJSONObject(i).getString("year"));
                    event.setDescendant(elementsList.getJSONObject(i).getString("descendant"));

                    events.add(event);
                    if (!types.containsKey(event.getDescription())) {
                        types.put(event.getDescription(), color);
                        color++;
                    }
                    if (event.getPersonID().equals(FamilyMap.SINGLETON.getUser().getUserId())) {
                        if (event.getDescription().equals("birth")) {
                            latitude1 = event.getLatitude();
                            longitude1 = event.getLongitude();
                        }
                    }
                }
                FamilyMap.SINGLETON.setEvents(events);
                FamilyMap.SINGLETON.setTypes(types);
                FamilyMap.SINGLETON.filterInitializer();
                getMarkers(types);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void getPeople(){
        URL url = null;

        try{
            url = new URL("http://" + FamilyMap.SINGLETON.getUser().getHostIp() + ":" +
                    FamilyMap.SINGLETON.getUser().getPort() + "/person/");
            PeopleTask ptask = new PeopleTask();
            ptask.setAuthorizationToken(FamilyMap.SINGLETON.getUser().getAuthorizationSer());
            ptask.execute(url);
            String wait = ptask.get();
            if(!wait.contains("message")) {
                JSONObject elements = new JSONObject(wait);
                JSONArray elementsList;
                List<People> peopleList = new ArrayList<>();

                elementsList = elements.getJSONArray("data");
                for (int i = 0; i < elementsList.length(); i++) {
                    People people = new People();
                    if(elementsList.getJSONObject(i).has("personID")){
                        people.setPersonID(elementsList.getJSONObject(i).getString("personID"));
                    }
                    if(elementsList.getJSONObject(i).has("firstName")){
                        people.setFirstName(elementsList.getJSONObject(i).getString("firstName"));
                    }
                    if(elementsList.getJSONObject(i).has("lastName")){
                        people.setLastName(elementsList.getJSONObject(i).getString("lastName"));
                    }
                    if(elementsList.getJSONObject(i).has("gender")){
                        people.setGender(elementsList.getJSONObject(i).getString("gender"));
                    }
                    if(elementsList.getJSONObject(i).has("spouse")){
                        people.setSpouse(elementsList.getJSONObject(i).getString("spouse"));
                    }
                    if(elementsList.getJSONObject(i).has("father")){
                        people.setFather(elementsList.getJSONObject(i).getString("father"));
                    }
                    if(elementsList.getJSONObject(i).has("mother")){
                        people.setMother(elementsList.getJSONObject(i).getString("mother"));
                    }

                    peopleList.add(people);
                }
                FamilyMap.SINGLETON.setPeople(peopleList);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.map_fragment, container, false);
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.recycle_map);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleMap = mMapView.getMap();

        if(FamilyMap.SINGLETON.getEvent().getEventID().equals("NoEvent")){
            getEvents();
            getPeople();
            FamilyMap.SINGLETON.getEvent().setLatitude(latitude1);
            FamilyMap.SINGLETON.getEvent().setLongitude(longitude1);
        } else {
            getMarkers(FamilyMap.SINGLETON.getTypes());
        }
        updateList(FamilyMap.SINGLETON.getEvent().getEventID());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(Double.parseDouble(FamilyMap.SINGLETON.getEvent().getLatitude())
                        , Double.parseDouble(FamilyMap.SINGLETON.getEvent().getLongitude()))).zoom(1).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker arg0) {

                updateList(arg0.getTitle());
                return true;
            }
        });

        // Perform any camera updates here
        return v;
    }

    public void updateList(String title){

        adapter = new MapAdapter(getActivity(), listItems);
        mRecyclerView.setAdapter(adapter);

        adapter.clearAdapter();

        MapItem item;
        item = new MapItem();
        if (!title.equals("NoEvent")) {
            for (int i = 0; i < FamilyMap.SINGLETON.getEvents().size(); i++) {
                Drawable genderIcon = null;
                if (FamilyMap.SINGLETON.getEvents().get(i).getEventID().equals(title)) {
                    for (int j = 0; j < FamilyMap.SINGLETON.getPeople().size(); j++) {
                        if (FamilyMap.SINGLETON.getPeople().get(j).getPersonID().equals(
                                FamilyMap.SINGLETON.getEvents().get(i).getPersonID())) {
                            if (FamilyMap.SINGLETON.getPeople().get(j).getGender().equals("m")) {
                                genderIcon = new IconDrawable(getActivity(), Iconify.IconValue.fa_male)
                                        .colorRes(R.color.colorPrimary).sizeDp(40);
                            } else {
                                genderIcon = new IconDrawable(getActivity(), Iconify.IconValue.fa_female)
                                        .colorRes(R.color.colorAccent).sizeDp(40);
                            }
                            item.setStext(FamilyMap.SINGLETON.getPeople().get(j).getFirstName() + " " +
                                    FamilyMap.SINGLETON.getPeople().get(j).getLastName() + "\n" +
                                    FamilyMap.SINGLETON.getEvents().get(i).getDescription() + ": " +
                                    FamilyMap.SINGLETON.getEvents().get(i).getCity() + " - " +
                                    FamilyMap.SINGLETON.getEvents().get(i).getCountry() + " (" +
                                    FamilyMap.SINGLETON.getEvents().get(i).getYear() + ")");
                            item.setImage(genderIcon);
                            item.setEvent(FamilyMap.SINGLETON.getEvents().get(i));
                            item.setPeople(FamilyMap.SINGLETON.getPeople().get(j));
                            listItems.add(item);
                            break;
                        }
                    }
                }
            }
        } else {
            Drawable mapMarker= new IconDrawable(getActivity(), Iconify.IconValue.fa_map_marker)
                    .colorRes(R.color.colorGray).sizeDp(40);
            item.setImage(mapMarker);
            item.setStext("Click on an marker to see event details");
            listItems.add(item);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        if (FamilyMap.SINGLETON.getEvent().getEventID().equals("NoEvent")){
            inflater.inflate(R.menu.menu_main, menu);
            super.onCreateOptionsMenu(menu, inflater);
        } else {
            inflater.inflate(R.menu.menu_map, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent itemIntent;
        if (id == R.id.action_settings){
            itemIntent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(itemIntent);
            return true;
        } else if (id == R.id.action_filter){
            itemIntent = new Intent(getActivity(), FilterActivity.class);
            startActivity(itemIntent);
            return true;
        } else if (id == R.id.action_search){
            itemIntent = new Intent(getActivity(), SearchActivity.class);
            startActivity(itemIntent);
            return true;
        } else if (id == R.id.action_up) {
            getActivity().finish();
            return true;
        } else if (id == R.id.action_top){
            itemIntent = new Intent(getActivity(), MainActivity.class);
            itemIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(itemIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}