package weverson.familymap.views.Recycler.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import weverson.familymap.R;

/**
 * Created by Weverson on 4/10/2016.
 */
public class SearchHolder extends RecyclerView.ViewHolder{
    public ImageView sImage;
    public TextView sText;
    public RelativeLayout sLayout;

    private void setupVariables(View view){
        sImage = (ImageView) view.findViewById(R.id.image1);
        sText = (TextView) view.findViewById(R.id.text1);
        sLayout = (RelativeLayout) view.findViewById(R.id.list_layout);

    }

    public SearchHolder(View view){
        super(view);
        setupVariables(view);

        view.setClickable(true);
    }
}
