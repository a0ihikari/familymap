package weverson.familymap.views.Recycler.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import weverson.familymap.R;
import weverson.familymap.views.Activity.FilterActivity;
import weverson.familymap.views.Activity.PersonActivity;
import weverson.familymap.views.Recycler.Holders.SearchHolder;
import weverson.familymap.views.Recycler.Items.SearchItems;

/**
 * Created by Weverson on 4/10/2016.
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchHolder>{
    private List<SearchItems> listItems;
    private Context mContext;
    private int focusedItem = 0;

    public SearchAdapter(Context context, List<SearchItems> _listItems){
        listItems = _listItems;
        mContext = context;
    }

    @Override
    public SearchHolder onCreateViewHolder (final ViewGroup viewGroup, int position){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list, null);
        final SearchHolder holder = new SearchHolder(v);
        holder.sLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(holder.sText.getText().toString().contains(":")){
                    Intent intent = new Intent(mContext, FilterActivity.class);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, PersonActivity.class);
                    mContext.startActivity(intent);
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final SearchHolder searchHolder, int position){
        SearchItems _listItems = listItems.get(position);
        searchHolder.itemView.setSelected(focusedItem == position);

        searchHolder.getLayoutPosition();

        searchHolder.sImage.setImageDrawable(_listItems.getImage());

        searchHolder.sText.setText(_listItems.getStext());
    }

    public void clearAdapter(){
        listItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return (null != listItems ? listItems.size(): 0);
    }
}
