package weverson.familymap.views.Recycler.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import weverson.familymap.R;
import weverson.familymap.models.Event;
import weverson.familymap.models.People;

/**
 * Created by Weverson on 4/10/2016.
 */
public class FilterHolder extends RecyclerView.ViewHolder{

    public TextView sTitle;
    public Switch aSwitch;
    public TextView sText;


    private void setupVariables(View view){
        sText = (TextView) view.findViewById(R.id.filter_text);
        sTitle = (TextView) view.findViewById(R.id.filter_title);
        aSwitch = (Switch) view.findViewById(R.id.switch_filter);
    }

    public FilterHolder(View view){
        super(view);
        setupVariables(view);

        view.setClickable(true);
    }
}
