package weverson.familymap.views.Recycler.Items;

import android.graphics.drawable.Drawable;
import android.widget.Switch;

import weverson.familymap.models.Event;
import weverson.familymap.models.People;

/**
 * Created by Weverson on 4/10/2016.
 */
public class FilterItems {
    private String stext;
    private String stitle;
    private Event event;
    private People people;
    private boolean aSwitch;

    public String getStitle() {
        return stitle;
    }

    public void setStitle(String stitle) {
        this.stitle = stitle;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public boolean getSwitch() {
        return aSwitch;
    }

    public void setSwitch(boolean aSwitch) {
        this.aSwitch = aSwitch;
    }

    public String getStext() {
        return stext;
    }

    public void setStext(String stext) {
        this.stext = stext;
    }
}
