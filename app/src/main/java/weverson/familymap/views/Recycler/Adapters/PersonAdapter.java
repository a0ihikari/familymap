package weverson.familymap.views.Recycler.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.plus.model.people.Person;

import java.util.List;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Activity.FilterActivity;
import weverson.familymap.views.Activity.MapActivity;
import weverson.familymap.views.Activity.PersonActivity;
import weverson.familymap.views.Recycler.Holders.PersonHolder;
import weverson.familymap.views.Recycler.Holders.SearchHolder;
import weverson.familymap.views.Recycler.Items.PersonItems;
import weverson.familymap.views.Recycler.Items.SearchItems;

/**
 * Created by Weverson on 4/10/2016.
 */
public class PersonAdapter extends RecyclerView.Adapter<PersonHolder>{
    private List<PersonItems> listItems;
    private Context mContext;
    private int focusedItem = 0;

    public PersonAdapter(Context context, List<PersonItems> _listItems){
        listItems = _listItems;
        mContext = context;
    }

    @Override
    public PersonHolder onCreateViewHolder (final ViewGroup viewGroup, int position){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list, null);
        final PersonHolder holder = new PersonHolder(v);
        holder.sLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(holder.sText.getText().toString().contains(":")){
                    Intent intent = new Intent(mContext, MapActivity.class);
                    FamilyMap.SINGLETON.setEvent(holder.event);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, PersonActivity.class);
                    FamilyMap.SINGLETON.setPerson(holder.people);
                    mContext.startActivity(intent);
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final PersonHolder personHolder, int position){
        PersonItems _listItems = listItems.get(position);
        personHolder.itemView.setSelected(focusedItem == position);

        personHolder.getLayoutPosition();

        personHolder.sImage.setImageDrawable(_listItems.getImage());

        personHolder.sText.setText(_listItems.getStext());

        personHolder.event = _listItems.getEvent();

        personHolder.people = _listItems.getPeople();
    }

    public void clearAdapter(){
        listItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return (null != listItems ? listItems.size(): 0);
    }
}
