package weverson.familymap.views.Recycler.Items;

import android.graphics.drawable.Drawable;

import weverson.familymap.models.Event;
import weverson.familymap.models.People;

/**
 * Created by Weverson on 4/10/2016.
 */
public class SearchItems {
    private String stext;
    private Drawable image;
    private Event event;
    private People people;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getStext() {
        return stext;
    }

    public void setStext(String stext) {
        this.stext = stext;
    }
}
