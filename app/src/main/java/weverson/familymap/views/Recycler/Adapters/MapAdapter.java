package weverson.familymap.views.Recycler.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Activity.FilterActivity;
import weverson.familymap.views.Activity.PersonActivity;
import weverson.familymap.views.Recycler.Holders.MapHolder;
import weverson.familymap.views.Recycler.Items.MapItem;

/**
 * Created by Weverson on 4/10/2016.
 */
public class MapAdapter extends RecyclerView.Adapter<MapHolder>{
    private List<MapItem> listItems;
    private Context mContext;
    private int focusedItem = 0;

    public MapAdapter(Context context, List<MapItem> _listItems){
        listItems = _listItems;
        mContext = context;
    }

    @Override
    public MapHolder onCreateViewHolder (final ViewGroup viewGroup, int position){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list, null);
        final MapHolder holder = new MapHolder(v);
        holder.sLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(!holder.sText.getText().toString().contains("an marker")){
                    Intent intent = new Intent(mContext, PersonActivity.class);
                    FamilyMap.SINGLETON.setEvent(holder.event);
                    FamilyMap.SINGLETON.setPerson(holder.people);
                    mContext.startActivity(intent);
                }

            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final MapHolder mapHolder, int position){
        MapItem _listItems = listItems.get(position);
        mapHolder.itemView.setSelected(focusedItem == position);

        mapHolder.getLayoutPosition();

        mapHolder.sImage.setImageDrawable(_listItems.getImage());

        mapHolder.sText.setText(_listItems.getStext());

        mapHolder.event = _listItems.getEvent();

        mapHolder.people = _listItems.getPeople();
    }

    public void clearAdapter(){
        listItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return (null != listItems ? listItems.size(): 0);
    }
}
