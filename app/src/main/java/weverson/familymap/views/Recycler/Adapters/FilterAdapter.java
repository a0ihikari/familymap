package weverson.familymap.views.Recycler.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.List;

import weverson.familymap.R;
import weverson.familymap.models.FamilyMap;
import weverson.familymap.views.Activity.FilterActivity;
import weverson.familymap.views.Activity.PersonActivity;
import weverson.familymap.views.Recycler.Holders.FilterHolder;
import weverson.familymap.views.Recycler.Holders.SearchHolder;
import weverson.familymap.views.Recycler.Items.FilterItems;
import weverson.familymap.views.Recycler.Items.SearchItems;

/**
 * Created by Weverson on 4/10/2016.
 */
public class FilterAdapter extends RecyclerView.Adapter<FilterHolder>{
    private List<FilterItems> listItems;
    private Context mContext;
    private int focusedItem = 0;

    public FilterAdapter(Context context, List<FilterItems> _listItems){
        listItems = _listItems;
        mContext = context;
    }

    @Override
    public FilterHolder onCreateViewHolder (final ViewGroup viewGroup, int position){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_list, null);
        final FilterHolder holder = new FilterHolder(v);
        holder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    for (String type : FamilyMap.SINGLETON.getTypes().keySet()){
                        if (holder.sTitle.getText().toString().contains(type)){
                            FamilyMap.SINGLETON.filter(type);
                            FamilyMap.SINGLETON.getFilterTypes().put(type,true);
                        }
                    }
                }


            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final FilterHolder filterHolder, int position) {
        FilterItems _listItems = listItems.get(position);
        filterHolder.itemView.setSelected(focusedItem == position);

        filterHolder.getLayoutPosition();

        filterHolder.sText.setText(_listItems.getStext());

        filterHolder.sTitle.setText(_listItems.getStitle());

        filterHolder.aSwitch.setChecked(_listItems.getSwitch());
    }

    public void clearAdapter(){
        listItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return (null != listItems ? listItems.size(): 0);
    }
}
