package weverson.familymap.views.Recycler.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import weverson.familymap.R;
import weverson.familymap.models.Event;
import weverson.familymap.models.People;

/**
 * Created by Weverson on 4/10/2016.
 */
public class PersonHolder extends RecyclerView.ViewHolder{
    public ImageView sImage;
    public TextView sText;
    public RelativeLayout sLayout;
    public Event event;
    public People people;

    private void setupVariables(View view){
        sImage = (ImageView) view.findViewById(R.id.image1);
        sText = (TextView) view.findViewById(R.id.text1);
        sLayout = (RelativeLayout) view.findViewById(R.id.list_layout);

    }

    public PersonHolder(View view){
        super(view);
        setupVariables(view);

        view.setClickable(true);
    }
}
