package weverson.familymap.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Weverson on 3/26/2016.
 */
public class PersonTask extends AsyncTask<URL, Void, String> {

    private Context context;
    private String authorizationSer;

    public void setContext(Context _context){
        context = _context;
    }

    public void setAuthorizationToken (String _authorizationSer){
        authorizationSer =_authorizationSer;
    }

    protected String doInBackground(URL... url) {
        String responseBodyData = "";

        for (int i = 0; i < url.length; i++) {

            try {
                HttpURLConnection connection = (HttpURLConnection) url[i].openConnection();
                connection.setRequestMethod("GET");

                // Set HTTP request headers, if necessary
                connection.addRequestProperty("Authorization", authorizationSer);
                connection.connect();
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get HTTP response headers, if necessary
                    // Map<String, List<String>> headers = connection.getHeaderFields();

                    // Get response body input stream
                    InputStream responseBody = connection.getInputStream();

                    // Read response body bytes
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int length = 0;
                    while ((length = responseBody.read(buffer)) != -1) {
                        baos.write(buffer, 0, length);
                    }
                    // Convert response body bytes to a string
                    responseBodyData = baos.toString();
                }
                connection.disconnect();
            }
            catch (Exception e) {
                Log.e("HttpClient", e.getMessage(), e);
            }
        }
        return responseBodyData;
    }

    protected void onPostExecute(String json) {
        try {
            JSONObject elements = new JSONObject(json);
            if(!elements.has("message")){
                String fName = elements.getString("firstName");
                String lName = elements.getString("lastName");

                Toast.makeText(context, "Hello " + fName + " " + lName +
                        "!", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(context, "Incorrect user name or password!", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
