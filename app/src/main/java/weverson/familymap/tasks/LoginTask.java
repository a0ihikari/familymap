package weverson.familymap.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import weverson.familymap.models.FamilyMap;
import weverson.familymap.models.User;

/**
 * Created by Weverson on 3/26/2016.
 */
public class LoginTask extends AsyncTask<URL, Void, String> {

    private String authorizationSer;
    private String personIdSer;
    private Context context;
    private String userName;
    private String password;
    private String hostIp;
    private String port;

    public void setContext(Context _context){
        context = _context;
    }

    public void setLogin (String _userName, String _password, String _hostIp, String _port){
        userName =_userName;
        password = _password;
        hostIp = _hostIp;
        port = _port;
    }

    protected String doInBackground(URL... url) {
        String responseBodyData = "";

        for (int i = 0; i < url.length; i++) {

            try {
                String postData = "{\n" +
                        "\tusername:\"" + userName + "\",\n" +
                        "\tpassword:\"" + password + "\"\n" +
                        "}";
                HttpURLConnection connection = (HttpURLConnection) url[i].openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);

                // Set HTTP request headers, if necessary
                // connection.addRequestProperty(”Accept”, ”text/html”)
                connection.connect();
                // Write post data to request body
                OutputStream requestBody = connection.getOutputStream();
                requestBody.write(postData.getBytes());
                requestBody.close();
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get HTTP response headers, if necessary
                    // Map<String, List<String>> headers = connection.getHeaderFields();

                    // Get response body input stream
                    InputStream responseBody = connection.getInputStream();

                    // Read response body bytes
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int length = 0;
                    while ((length = responseBody.read(buffer)) != -1) {
                        baos.write(buffer, 0, length);
                    }

                    // Convert response body bytes to a string
                    responseBodyData = baos.toString();
                }
                connection.disconnect();
            }
            catch (Exception e) {
                Log.e("HttpClient", e.getMessage(), e);
                if (e.getMessage().contains("EHOSTUNREACH")){
                    Toast.makeText(context, "Verify your host and port!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return responseBodyData;
    }

    protected void onPostExecute(String json) {
        try {
            JSONObject elements = new JSONObject(json);
            if(!elements.has("message")){
                String userId = elements.getString("personId");
                authorizationSer = elements.getString("Authorization");
                personIdSer = elements.getString("personId");
                URL url = new URL("http://" + hostIp + ":" + port + "/person/" + personIdSer);
                PersonTask ptask = new PersonTask();
                ptask.setContext(context);
                ptask.setAuthorizationToken(authorizationSer);
                ptask.execute(url);

                User user = new User();
                user.setAuthorizationSer(authorizationSer);
                user.setHostIp(hostIp);
                user.setPort(port);
                user.setUserId(userId);
                FamilyMap.SINGLETON.setUser(user);

            } else {
                Toast.makeText(context, "Incorrect user name or password!", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
